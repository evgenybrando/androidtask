package ru.omsu.imit.androidtasks

import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*

const val SECRET_KEY_FOR_NAME = "ru.omsu.imit.androidtasks.SECRET_kEY"

private const val KEY_FLOPPA_COUNT = "FLOPPA_COUNT"
private const val KEY_NAME = "NAME"
private const val WATCHTOWER_NOTIFY_ID = 1488
private const val CHANNEL_ID = "My app ID"

class MainActivity : AppCompatActivity() {
    private var floppaCount = 0
    private var name = ""

    private fun onExitNotification() {
        val nIntent = Intent(this, AboutWatchtowerActivity::class.java)
        val contentIntent =
            PendingIntent.getActivity(this, 0, nIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_jimi))
            .setContentTitle(getString(R.string.the_watchtower))
            .setContentText(getString(R.string.watchtower_notification_text))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(contentIntent)
            .setVibrate(longArrayOf(500, 500, 500))
            .setAutoCancel(true)
            .build()

        NotificationManagerCompat.from(this).notify(WATCHTOWER_NOTIFY_ID, notification)
    }

    private fun onCreateToastNotification() {
        //  Create toast in program (Use custom toasts considered bad practice)
        val toastImg = ImageView(this).apply {
            setImageResource(R.drawable.floppa1)
        }

        Toast.makeText(applicationContext, R.string.app_greeting, Toast.LENGTH_LONG).apply {
            setGravity(Gravity.CENTER, 0, 0)
            (view as LinearLayout).addView(toastImg, 0)
        }.show()
    }

    private fun openQuitDialog() {
        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.quit_confirm))
            setPositiveButton(getString(R.string.yes)) { _, _ -> finish() }
            setNegativeButton(getString(R.string.no)) { _, _ -> }
        }.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mysteryCatImageButton.setOnClickListener {
            name = with(enterNameEditText.text) { if (isEmpty()) getString(R.string.stranger) else toString() }

            greetingTextView.text = getString(R.string.cat_greeting).format(name)
        }

        crowCountButton.setOnClickListener {
            crowCountTextView.text = getString(R.string.floppa_count_format_str).format(++floppaCount)
        }

        NotificationManagerCompat.from(this).cancel(WATCHTOWER_NOTIFY_ID)

        if (savedInstanceState != null) {
            floppaCount = savedInstanceState.getInt(KEY_FLOPPA_COUNT)
            crowCountTextView.text = getString(R.string.floppa_count_format_str).format(floppaCount)
            name = savedInstanceState.getString(KEY_NAME).toString()
            greetingTextView.text = getString(R.string.cat_greeting).format(name)

        } else {
            onCreateToastNotification()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(KEY_FLOPPA_COUNT, floppaCount)
        outState.putString(KEY_NAME, name)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_the_watchtower -> {
                val intent = Intent(this, AboutWatchtowerActivity::class.java)
                startActivity(intent)

                return true
            }
            R.id.action_to_traffic_lights -> {
                val intent = Intent(this, TrafficLightsActivity::class.java).apply {
                    if (name.isNotEmpty()) {
                        putExtra(SECRET_KEY_FOR_NAME, name)
                    }
                }
                startActivity(intent)

                return true
            }
            R.id.action_to_converter -> {
                val intent = Intent(this, ConverterActivity::class.java)
                startActivity(intent)

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        openQuitDialog()
    }

    override fun onStop() {
        super.onStop()

        onExitNotification()
    }
}